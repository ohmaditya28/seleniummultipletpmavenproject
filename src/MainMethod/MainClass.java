package MainMethod;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Arrays;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import CommonMethod.AddConditionRTTool;
import CommonMethod.AddFilterConditons;
import CommonMethod.AddFilters;
import CommonMethod.BrowserLaunch;
import CommonMethod.ExecutionPDFReportSave;
import CommonMethod.RTToolCertificationReport;
import CommonMethod.TechMReportGlobalLogin;
import CommonMethod.WaittillElementFound;
import ObjectRepository.CLMURLRepository;

public class  MainClass extends CLMURLRepository
{
	
public static void main(String[] args) throws Exception 
{
	Logger logger = Logger.getLogger("devpinoyLogger");
	
	System.setProperty("webdriver.gecko.driver","C:\\Automation_Pract\\Selenium\\MozilaDriver\\geckodriver.exe");
	
	DesiredCapabilities capabilities = new DesiredCapabilities();
	capabilities = DesiredCapabilities.firefox();
	capabilities.setBrowserName("firefox");
	capabilities.setVersion("your firefox version");
    capabilities.setPlatform(Platform.WINDOWS);
    capabilities.setCapability("marionette", false);
    
   
	 
	 @SuppressWarnings("deprecation")
	WebDriver driver = new FirefoxDriver(capabilities);

	 
	driver.manage().window().maximize();
	System.out.println(CertificationReport);
	driver.get(CertificationReport);
	Thread.sleep(2000);
	logger.debug("URL Launched");

	Thread.sleep(700);
	TechMReportGlobalLogin TRGL=new TechMReportGlobalLogin();
	RTToolCertificationReport RTTC=new RTToolCertificationReport();
	WaittillElementFound WAITTILL=new WaittillElementFound();
	AddFilters Adf=new AddFilters();
	AddConditionRTTool ACT=new AddConditionRTTool();
	AddFilterConditons AFC=new AddFilterConditons();
	ExecutionPDFReportSave EPDGRS=new ExecutionPDFReportSave();

	
	//Main Program
	if(driver.findElement(By.id("lblUserID")).isDisplayed())
	{
		System.out.println("Landed on Correct Page");
		Thread.sleep(300);
		TRGL.ReadData2(0);
		
		
	}
	else
	{
		System.out.println("Not Landed on Correct Page");
	}
	
	TRGL.TechMLogin(driver);
	
	WAITTILL.RTToolPageCome(driver);
	
	RTTC.RTCertification(driver);

	Adf.ClickDataAddFilter(driver);
	
	ACT.AddConditions(driver);
	Thread.sleep(500);
	AFC.SelectFilterWorkArea(driver);
	AFC.ReadData1(0);
	AFC.WorkAreaValue(driver);
	ACT.AddConditions(driver);
	AFC.CertificationReportGridChanges(driver);
	Thread.sleep(1000);
	AFC.SelectFilterTPID(driver);
	AFC.TPIDValue(driver);
	Thread.sleep(1000);
	//AFC.CertificationReportGridChanges(driver);
	Thread.sleep(2000);
	Adf.ClickReportHomeExportButton(driver);
	Adf.ReportGenerationSwitchWindow(driver);
	EPDGRS.ReadData3(0);
	EPDGRS.ReportSaveinWindow(driver);
	

    
	
	
	
}
}

