package ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CLMObjectRepository
{
	WebDriver d=null;
public void DefectDepositionCRMLogin()
{
	WebElement DefectDepUsername=d.findElement(By.name("j_username"));
	WebElement DefectDepPassword=d.findElement(By.name("j_password"));
	WebElement DefectDepLoginbtn=d.findElement(By.xpath("//button[.,'Log In']"));
	}


public void ATTGlobalLogin()
{
	WebElement ATTIdUsername=d.findElement(By.name("userid"));
	WebElement ATTidPassword=d.findElement(By.name("password"));
	WebElement ATTidOkBtn=d.findElement(By.name("btnSubmit"));
	WebElement ATTidLogonokBtn=d.findElement(By.name("successOK"));
}

public void RTTPostLogin()
{
	WebElement TechMRToolsClickEvent=d.findElement(By.xpath("//a[@class='mstrLargeIconViewItemLink' or contains(href,'href=\"mstrWeb?evt=3010&src=mstrWeb.3010&ServerAlias=LLPD033.CHDC.ATT.COM&Port=34962&Project=RT_Tools&Server=LLPD033.CHDC.ATT.COM&loginReq=true\"')]"));
	WebElement TechMRTToolsSharedReportClickEvent=d.findElement(By.xpath("//div[@class='mstr-dskt-nm' and contains(.,'Shared Reports')]"));
	WebElement TechMRTToolsTPSummaryReport=d.findElement(By.xpath("//a[@class='mstrLink' and contains(.,'TP_Summary_Report')]"));
}


public void AddViewFilterReportpage()
{

WebElement TechMRToolsDataMenuClick=d.findElement(By.xpath("//td[@class='mstrListBlockToolbarItemName']/descendant::div[contains(.,'Data')]"));
WebElement TechMRToolsDataMenuAddnewFilterClick=d.findElement(By.xpath("//div[@class='mstrMenuOption_d2']/descendant::span[contains(.,'Add View Filter Condition...')]"));
	
}

public void AddFilterOnComboBox(String ValuetoSelect)
{
Select cmbboxFilteron=new Select(d.findElement(By.name("objectIDTypeNameCombo")));	

cmbboxFilteron.selectByVisibleText(ValuetoSelect);
}

public void ReportHomeMenuGeneration(String Exportoptions)
{
	WebElement ReportHomeMenuClick=d.findElement(By.xpath("//td[@class='mstrListBlockToolbarItemName' and contains(.,'Report Home')]"));
	WebElement ReportHomeMenuExportClick=d.findElement(By.xpath("//span[contains(.,'Export')]"));
	WebElement ReportHomeMenuExportOptions=d.findElement(By.xpath("//div[@class='mstrMenuOption_d2']/descendant::span[contains(.,'"+Exportoptions+"')]"));
	
}

public void newWindowReportGeneration(String ScalingAdjustTo,String ScalingFitTo,String Orientation)
{
	
	WebElement ExportBtn=d.findElement(By.name("3131"));
	
}


public void DefectDepositionReportBuilder()
{
	
	WebElement DefectDepDuplicatebtn=d.findElement(By.id("duplicate-report-button"));
	
}

	
}
