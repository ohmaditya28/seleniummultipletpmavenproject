package CommonMethod;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.Key;

public class TPSummaryReportPDFOptions
{
public void TPSummaryReport_ScalingRadioBtn(WebDriver d) throws Exception
{
	d.findElement(By.xpath("//input[@id='PDFScalingOption' and @value='1']")).click();
	Thread.sleep(1000);
	d.findElement(By.id("PDFFitToPagesWide")).clear();
	Thread.sleep(500);
	d.findElement(By.id("PDFFitToPagesWide")).sendKeys("1");

	Thread.sleep(500);
	d.findElement(By.id("PDFFitToPagesTall")).clear();
    Thread.sleep(500);
	d.findElement(By.id("PDFFitToPagesTall")).sendKeys("1");
	
	
	
	
}
}
