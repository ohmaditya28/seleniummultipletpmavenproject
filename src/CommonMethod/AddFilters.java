package CommonMethod;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class AddFilters extends WaittillElementFound
{
	Logger logger = Logger.getLogger("devpinoyLogger");
public void ClickDataAddFilter(WebDriver d) throws Exception
{
	WebElement DataMenuHover=d.findElement(By.xpath("//div[contains(.,'Data')]/following::td[@class='right menu']"));
	Actions actions =new Actions(d);
	actions.moveToElement(DataMenuHover).perform();
	Thread.sleep(300);
	actions.moveToElement(DataMenuHover).click().build().perform();
	Thread.sleep(2000);
	d.findElement(By.xpath("//div[@class='mstrMenuOption_d2']/preceding::span[contains(.,'Add View Filter Condition...')]")).click();
	Thread.sleep(3000);
	WebElement ClearAllHover=d.findElement(By.xpath("//input[@class='mstrIcon-btn mstrIcon-btnClearFilter']"));
	actions.moveToElement(ClearAllHover).perform();
	Thread.sleep(300);
	actions.moveToElement(ClearAllHover).click().build().perform();
	RTToolFilterEmpty(d);
}

public void ClickReportHomeExportButton(WebDriver d) throws Exception
{
	
	try
	{
		Actions actions=new Actions(d);
		RTToolReportHomeExport(d);
		WebElement PDFClicked=d.findElement(By.xpath("//a[@class='mstrToolbarButton']/span[@id='tbPDF']"));
		actions.moveToElement(PDFClicked).perform();
		Thread.sleep(2000);
		actions.moveToElement(PDFClicked).click().perform();
		
	}
	catch(Exception e)
	{
		System.out.println(e.getStackTrace());
		System.out.println(e);
	}

}

public void ReportGenerationSwitchWindow(WebDriver d) throws Exception
{
	String ParentReportGeneratedWindow=d.getWindowHandle();	
	for (String SubWindowExportWindow:d.getWindowHandles())
	{
		d.switchTo().window(SubWindowExportWindow);
	}
	RTToolReportHomeExportPageLoading(d);
	TPSummaryReportPDFOptions TPSR=new TPSummaryReportPDFOptions();
	TPSR.TPSummaryReport_ScalingRadioBtn(d);
	Thread.sleep(500);
	d.findElement(By.id("3131")).click();
	Thread.sleep(500);
	
	}


	
}
