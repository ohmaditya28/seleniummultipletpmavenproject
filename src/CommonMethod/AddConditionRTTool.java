package CommonMethod;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class AddConditionRTTool extends WaittillElementFound
{
public void AddConditions(WebDriver d) throws Exception
{
	Actions actions=new Actions(d);
	
	WebElement AddConditionsHover=d.findElement(By.xpath("//img[@class='mstrIcon-btn mstrIcon-btnAddCondition']"));
	
	actions.moveToElement(AddConditionsHover).perform();
	Thread.sleep(500);
	actions.moveToElement(AddConditionsHover).click().build().perform();
	RTToolAddConditionFilter(d);
		
}

	
	
}
