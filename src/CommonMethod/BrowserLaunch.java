package CommonMethod;

import java.util.Arrays;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import ObjectRepository.CLMURLRepository;

public class BrowserLaunch extends CLMURLRepository
{
		
public void GoogleChromeLaunch(WebDriver d)
{
	
	System.setProperty("webdriver.chrome.driver","C:\\Automation_Pract\\Selenium\\chromedriver.exe");
	DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	capabilities.setCapability("chrome.switches", Arrays.asList("--incognito"));
	d = new ChromeDriver(capabilities);
	d.manage().window().maximize();
	d.get(super.CertificationReport);
}

public void VerficationGlobalLoginScreen(WebDriver d)
{
	if(d.findElement(By.id("lblUserID")).isDisplayed())
	{
		System.out.print("Test Case is Passed");
	}
	else
	{
		System.out.print("Test Case is not  Passed");
	}
	d.quit();
}



}


