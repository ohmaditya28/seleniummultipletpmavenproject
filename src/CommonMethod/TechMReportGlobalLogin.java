package CommonMethod;

import java.io.File;
import java.io.FileInputStream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ObjectRepository.CLMObjectRepository;


public class TechMReportGlobalLogin
{
	Logger log = Logger.getLogger("devpinoyLogger");
	

public ArrayList<String> TechMLogin(WebDriver d) throws Exception
{
	ArrayList<String> UserName=ReadData2(0);
	ArrayList<String> Password=ReadData2(1);
	
	for(int i=0;i<UserName.size();i++)
	{
		try {
			
		
		d.findElement(By.name("userid")).sendKeys(UserName.get(i));
		log.debug("UserName is Entered");
		Thread.sleep(300);
		d.findElement(By.name("password")).sendKeys(Password.get(i));
		log.debug("Password is Entered");
		Thread.sleep(2000);
		d.findElement(By.name("btnSubmit")).click();
		Thread.sleep(1000);
		d.findElement(By.name("successOK")).click();
		log.debug("ok Button Clicked");
		Thread.sleep(700);
		log.debug("Login Successfull");
		}
		
		catch(Exception e)
		{
			System.out.println(e);
			String LoginError=d.findElement(By.xpath("//b[contains(.,'incorrect')]")).getText();
			log.error(LoginError);
			log.debug("Login Failed");
		}
		
			
}
	return Password;
	}
	
	
	

	
	public ArrayList<String> ReadData2(int ColNum) throws Exception
	{
		String filePath = System.getProperty("user.dir");   
	    String fileName="DataExcel.xlsx";
	    //String sheetName="test";
	    
	 File file =    new File(filePath+"\\"+fileName); 
	//FileInputStream fis=new FileInputStream("C:\\Selenium Automation Program\\Data\\DataExcel.xlsx");

	//XSSFWorkbook workbook=new XSSFWorkbook(fis);
	XSSFWorkbook workbook=new XSSFWorkbook(file);

	XSSFSheet sheet=workbook.getSheet("Credentials_Global");


	Iterator<Row> rowIterator=sheet.iterator();

	rowIterator.next();

	ArrayList<String> list=new ArrayList<String>();

	while(rowIterator.hasNext())
	{

		list.add(rowIterator.next().getCell(ColNum).getStringCellValue());
	}
	log.debug("Values from Excel Loaded in Collections");
	//System.out.println(list);
	return list;
	
	}	

	
	
}