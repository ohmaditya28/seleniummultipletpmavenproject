package CommonMethod;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.ImageLocator;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.sikuli.*;

public class ExecutionPDFReportSave extends WaittillElementFound
{

public ArrayList<String>  ReportSaveinWindow(WebDriver d) throws Exception
{
	RTToolSaveReport(d);
	Robot robot=new Robot();
	robot.keyPress(KeyEvent.VK_CONTROL);
	robot.keyPress(KeyEvent.VK_S);
	robot.keyRelease(KeyEvent.VK_CONTROL);
	robot.keyRelease(KeyEvent.VK_S);
	System.out.println("Report is going to Save");
	Thread.sleep(2000);
	
	Screen s=new Screen();
	Pattern SaveAsScreen=new Pattern("Images\\SaveAsScreen.PNG");
	s.wait(SaveAsScreen,10);
	
	Pattern OpenBtn=new Pattern("Images\\Savebutton.PNG");
	
	
	
	Pattern EnterFileName=new Pattern("Images\\FileNamewithoutData.PNG");
	
	
	System.out.println("Save As Screen displayed");
	logger.debug("Save As Screen displayed");
	logger.debug("Save As Screen displayed");
	Thread.sleep(1000);
	
	robot.keyPress(KeyEvent.VK_CONTROL);

	robot.keyPress(KeyEvent.VK_A);

	robot.keyRelease(KeyEvent.VK_CONTROL);

	robot.keyPress(KeyEvent.VK_BACK_SPACE);
	Thread.sleep(1000);
	ArrayList<String> TPIDReportName=ReadData3(1);
	
	s.type(EnterFileName,TPIDReportName.get(0));
	Thread.sleep(1000);
    s.click(OpenBtn);
    Thread.sleep(1000);
	return TPIDReportName;
 
	
	
}

public ArrayList<String> ReadData3(int ColNum) throws Exception
{
	 
	String filePath = System.getProperty("user.dir");   
   String fileName="DataExcel.xlsx";
   //String sheetName="test";
   
File file =    new File(filePath+"\\"+fileName); 
//FileInputStream fis=new FileInputStream("C:\\Selenium Automation Program\\Data\\DataExcel.xlsx");

//XSSFWorkbook workbook=new XSSFWorkbook(fis);
XSSFWorkbook workbook=new XSSFWorkbook(file);

XSSFSheet sheet=workbook.getSheet("TCCertification");


Iterator<Row> rowIterator=sheet.iterator();

rowIterator.next();

ArrayList<String> list=new ArrayList<String>();

while(rowIterator.hasNext())
{

	list.add(rowIterator.next().getCell(ColNum).getStringCellValue());
}
//System.out.println(list);
return list;




	
}	

	
}
