package CommonMethod;

import java.io.File;
import java.io.FileInputStream;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class AddFilterConditons extends WaittillElementFound 
{
	Logger logger = Logger.getLogger("devpinoyLogger");
	public void SelectFilterWorkArea(WebDriver d)
	{
		WebElement FilterOndropdown=d.findElement(By.name("objectIDTypeNameCombo"));
		Select FilterOn=new Select(FilterOndropdown);
		FilterOn.selectByVisibleText("WORK AREA");
		RTToolAddConditionWorkArea(d);
		d.findElement(By.xpath("//input[@id='262144004']")).click();
		RTToolAddConditionWorkAreaQuality(d);
		
		
		
	}
	public ArrayList<String> WorkAreaValue(WebDriver d) throws Exception
	{
		ArrayList<String> Work_Area=ReadData1(0);
		System.out.println(Work_Area);
		d.findElement(By.id("constantValue")).sendKeys(Work_Area.get(0));	
		Thread.sleep(500);
		d.findElement(By.id("acceptButton")).click();
		Thread.sleep(2000);
		RTToolWorkAreaViewFilterVerification(d);
		
		return Work_Area;
		
		
	}
	
	public void  SelectFilterTPID(WebDriver d) throws Exception
	{
		
		WebElement FilterOndropdown=d.findElement(By.name("objectIDTypeNameCombo"));
		Select FilterOn=new Select(FilterOndropdown);
		FilterOn.selectByVisibleText("TP ID");
		RTToolAddConditionTPID(d);
		
		
	
			
		
	}
	
	public ArrayList<String> TPIDValue(WebDriver d) throws Exception
	{
		
		ArrayList<String> TPID=ReadData1(1);
		d.findElement(By.xpath("//input[@class='mstrIcon-btn mstrIcon-btnRadio']")).click();
		Thread.sleep(300);
		RTToolAddConditionTPIDQuality(d);
		
		WebElement TPIDEqualdropdown=d.findElement(By.xpath("//select[@name='functionAndFunctionTypeCombo']"));
		Select EqualFilterON=new Select(TPIDEqualdropdown);
		EqualFilterON.selectByVisibleText("In (enter value1;value2; ...;valueN)");
		Thread.sleep(1000);
		
		d.findElement(By.id("constantValue")).clear();
		Thread.sleep(300);
		d.findElement(By.id("constantValue")).sendKeys(TPID.get(0));	
		Thread.sleep(500);
		d.findElement(By.id("acceptButton")).click();
		RTToolTPFilterVerification(d);
				
		return TPID;
		
	
		
		
	}
	
	public void CertificationReportGridChanges(WebDriver d) throws Exception
	{
		
	    Actions actions=new Actions (d);
	   
		JavascriptExecutor js = (JavascriptExecutor)d;

		String javaScript = "var evt = document.createEvent('MouseEvents');"
		                + "var RIGHT_CLICK_BUTTON_CODE = 2;"
		                + "evt.initMouseEvent('contextmenu', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, RIGHT_CLICK_BUTTON_CODE, null);"
		                + "arguments[0].dispatchEvent(evt)";
		WebDriverWait wait=new WebDriverWait(d,20);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[@class='r-h']/following::td[contains(.,'TEST CASE TITLE')]")));
		System.out.print("TEST CASE TITLE");
		WebElement RCToolTestCaseTitleRightClick=d.findElement(By.xpath("//tr[@class='r-h']/following::td[contains(.,'TEST CASE TITLE')]"));
	    Thread.sleep(5000);
		js.executeScript(javaScript,RCToolTestCaseTitleRightClick);
		
		Thread.sleep(1000);
		
		WebElement RightClickRemovefromGrid=d.findElement(By.xpath("//td[contains(.,'Remove from Grid')]"));
		actions.moveToElement(RightClickRemovefromGrid).perform();
		Thread.sleep(1000);
		actions.moveToElement(RightClickRemovefromGrid).click().build().perform();
		Thread.sleep(6000);
		
		WebElement RCToolTestCaseLinkRightClick=d.findElement(By.xpath("//tr[@class='r-h']/following::td[contains(.,'Test Case Link')]"));
		actions.moveToElement(RCToolTestCaseLinkRightClick).perform();
		Thread.sleep(1000);
		actions.contextClick(RCToolTestCaseLinkRightClick).build().perform();
		Thread.sleep(1500);
		
		WebElement RightClickRemovefromGrid1=d.findElement(By.xpath("//td[contains(.,'Remove from Grid')]"));
		actions.moveToElement(RightClickRemovefromGrid1).perform();
		actions.moveToElement(RightClickRemovefromGrid1).click().build().perform();
		Thread.sleep(4000);
		
				
		
	}
	
	
	public ArrayList<String> ReadData1(int ColNum) throws Exception
	 {
		 
		String filePath = System.getProperty("user.dir");   
	    String fileName="DataExcel.xlsx";
	    //String sheetName="test";
	    
	 File file =    new File(filePath+"\\"+fileName); 
	//FileInputStream fis=new FileInputStream("C:\\Selenium Automation Program\\Data\\DataExcel.xlsx");

	//XSSFWorkbook workbook=new XSSFWorkbook(fis);
	XSSFWorkbook workbook=new XSSFWorkbook(file);

	XSSFSheet sheet=workbook.getSheet("TCCertification");


	Iterator<Row> rowIterator=sheet.iterator();

	rowIterator.next();

	ArrayList<String> list=new ArrayList<String>();

	while(rowIterator.hasNext())
	{

		list.add(rowIterator.next().getCell(ColNum).getStringCellValue());
	}
	//System.out.println(list);
	return list;
	



		
	 }	
	
}
