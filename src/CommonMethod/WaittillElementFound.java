package CommonMethod;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaittillElementFound 
{
	Logger logger = Logger.getLogger("devpinoyLogger");
public void RTToolPageCome(WebDriver d)
{
	WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Install MicroStrategy Office')]")));
	System.out.println("RT Tool is visible now");
	
	
}

public void RTTtoolSharedPage(WebDriver d)
{
	
	WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='mscld-create-menu']")));
	System.out.println("RT Tool Shared is visible now");
	//span[@class='mstrPathLast' and contains(.,'Shared Reports')]
}

public void RTToolTPSummaryReport(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mstrPathLast' and contains(.,'Shared Reports')]")));
	System.out.print("TPSummaryReport is visible now");
}


public void RTToolReportDetails(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mstrPathLast' and contains(.,'TP_Summary_Report')]")));
	System.out.println("ReportDetails is visible now");
	
}

public void RTToolAddviewFilter(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='tbViewFilter']")));
	System.out.println("ReportDetails is visible now");
}

public void RTToolFilterEmpty(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mstrPanelTitleButtonBar']/following-sibling::span[contains(.,'The filter is empty.')]")));
	System.out.println("The Filter is empty.. is visible now");
	
}
public void RTToolAddConditionFilter(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@name='objectIDTypeNameCombo']")));
	System.out.println("Filter On Drop box is shown");
}

public void RTToolAddConditionWorkArea(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mstr-filter-object-name' and contains(.,'WORK AREA')]")));
	System.out.println("Filter On Drop box is shown");
}

public void RTToolAddConditionWorkAreaQuality(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='constantValue']")));
	System.out.println("WorkArea Qualify Radio button is checked");
}

public void RTToolWorkAreaViewFilterVerification(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(.,'Equals')]")));
	System.out.println("WorkArea ID equals Node");	
}

public void RTToolAddConditionTPID(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='mstr-filter-object-name' and contains(.,'TP ID')]")));
	System.out.println("Filter On Drop box is shown");
}

public void RTToolAddConditionTPIDQuality(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='constantValue']")));
	System.out.println("TP ID Qualify Radio button is checked");
	logger.debug("TP ID Qualify Radio button is checked");
}

public void RTToolTPFilterVerification(WebDriver d)
{
WebDriverWait wait=new WebDriverWait(d,20);
	
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='toolbar-static-text']")));
	System.out.println("RowData Generated");
	logger.debug("RowData Generated");
	
	
}
public void RTToolReportHomeExport(WebDriver d)
{
	WebDriverWait wait=new WebDriverWait(d,20);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='mstrToolbarButton']/span[@id='tbPDF']")));
	System.out.println("Export PDF is visible");
	logger.debug("Export PDF is visible");
}

public void RTToolReportHomeExportPageLoading(WebDriver d)
{
	WebDriverWait wait=new WebDriverWait(d,30);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(.,'PDF Options')]")));
	System.out.println("Export Screen is visible");
	logger.debug("Export is visible");
}

public void RTToolSaveReport(WebDriver d)
{
	WebDriverWait wait=new WebDriverWait(d,30);
	wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(.,'TP_Summary_Report')]")));
	System.out.println("TP Summary Report is fully loaded");
	logger.debug("TP Summary Report is fully loaded");
}
	
}
