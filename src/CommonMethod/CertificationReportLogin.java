package CommonMethod;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CertificationReportLogin 
{
	
public ArrayList<String> ReadData1(int ColNum) throws Exception
	 {
		 
	String filePath = System.getProperty("user.dir");   
    String fileName="DataExcel.xlsx";
    //String sheetName="test";
    
 File file =    new File(filePath+"\\"+fileName); 
//FileInputStream fis=new FileInputStream("C:\\Selenium Automation Program\\Data\\DataExcel.xlsx");

//XSSFWorkbook workbook=new XSSFWorkbook(fis);
XSSFWorkbook workbook=new XSSFWorkbook(file);

	XSSFSheet sheet=workbook.getSheet("TCCertification");


	Iterator<Row> rowIterator=sheet.iterator();

	rowIterator.next();

	ArrayList<String> list=new ArrayList<String>();

	while(rowIterator.hasNext())
	{

		list.add(rowIterator.next().getCell(ColNum).getStringCellValue());
	}
	//System.out.println(list);
	return list;
	



		
	 }	
	

}
